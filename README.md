# MicroRender Serialiser

[![JSR](https://jsr.io/badges/@microrender/serialiser)](https://jsr.io/@microrender/serialiser)
[![JSR Score](https://jsr.io/badges/@microrender/serialiser/score)](https://jsr.io/@microrender/serialiser)

This is a serialiser designed for MicroRender to enable better serialisation of builtin and custom JS
objects than `JSON.stringify()` and `JSON.parse()`. One of the main focuses of the serialiser is code size,
so while other serialisers can serialise and deserialise a wider range of objects, this serialiser can do
it at a fraction of the code size.

## Usage

Deno / Deno-compatiable bundlers:

```javascript
import { serialise, deserialise } from "@jsr:microrender/serialiser"
```

Node / other runtimes:

```sh
npx jsr add @microrender/serialiser
```

```javascript
import { serialise, deserialise } from "@microrender/serialiser"
```

Usage:
```
serialise(object)
serialise(object, locals)

deserialise(string)
deserialise(string, locals)
```

## Examples

### Plain objects

```javascript
serialise({hello: "world"})
```

```
<00 00 00 22 02 06 4f 62 6a 65 63 74 00 00 00 0b 03 00 68 65 6c 6c 6f 00 00 00 0b 03 00 77 6f 72 6c 64>
```

### Arrays and Maps

```javascript
serialise([1, 2, 3, 4, 5])
serialise(new Map([["key", "value"], ["key2", "value2"]]))
```

```
<00 00 00 51 01 05 41 72 72 61 79 00 00 00 0e 03 01 3f f0 00 00 00 00 00 00 00 00 00 0e 03 01 40 00 00 00 00 00 00 00 00 00 00 0e 03 01 40 08 00 00 00 00 00 00 00 00 00 0e 03 01 40 10 00 00 00 00 00 00 00 00 00 0e 03 01 40 14 00 00 00 00 00 00>
<00 00 00 49 01 03 4d 61 70 00 00 00 1f 01 05 41 72 72 61 79 00 00 00 09 03 00 6b 65 79 00 00 00 0b 03 00 76 61 6c 75 65 00 00 00 21 01 05 41 72 72 61 79 00 00 00 0a 03 00 6b 65 79 32 00 00 00 0c 03 00 76 61 6c 75 65 32>
```

### URL and Date objects

```javascript
serialise(new URL("https://example.com/a/b/c"))
serialise(new Date(0))
```

```
<00 00 00 28 00 03 55 52 4c 00 00 00 1f 03 00 68 74 74 70 73 3a 2f 2f 65 78 61 6d 70 6c 65 2e 63 6f 6d 2f 61 2f 62 2f 63>
<00 00 00 28 00 04 44 61 74 65 00 00 00 1e 03 00 31 39 37 30 2d 30 31 2d 30 31 54 30 30 3a 30 30 3a 30 30 2e 30 30 30 5a>
```

## About

The MicroRender serialiser uses a binary encoding format for efficiency. All numbers are stored big-endian (saves
a few bytes of javascript)

 - Each *descriptor* (an item including a description of type and length) is made up a number of components:
   - Bytes 0-3: a Uint32 storing the length of the descriptor
   - Byte 4: a Uint8 storing the type of the descriptor:
     - `0`: string (primitive)
       - Stored as a variable-length UTF-8 string
     - `1`: number (primitive)
       - Stored as a Float64
     - `2`: bigint (primitive)
       - Stored as a BigInt64
     - `3`: boolean (primitive)
       - Stored as a Uint8 (0=false, 1=true)
     - `4`: undefined (primitive)
       - No data
     - `5`: null (primitive)
       - No data
     - `6`: ToJSON (composite)
       - A `toJSON()` has been called on the object being serialised.
       - This is a composite descriptor containing a single child descriptor, a string.
     - `7`: Iterable (composite)
       - The object being serialised was an iterable (has a `@@iterator()`) method.
       - This is a composite descriptor containing each iterable entry as each child descriptor.
       - Note that each item when iterating through JS `Map` is itself an `Array` so it is stored as an iterable
         of iterables.
     - `8`: BasicObject (composite)
       - The object being serialised does not have an specific ways to serialise it, so its own enumerable string-keyed
         properties were stored.
       - This is a composite descriptor containing each key and value as child descriptors (in key-value-key-value
         order)
       - Note that objects that store data using symbols or private (`#`) properties (eg. the `Request` object) cannot
         be serialised this way, and objects with mangled property names (added during minification) cannot be
         reliably serialised or deserialised this way either.
   - Bytes 5 onwards are dependent on the type of descriptor:
     - Composite descriptors:
       - Byte 5: a Uint8 storing the byte length of the object's constructor name
       - Bytes 6-(6+value of byte 4): a UTF-8 encoded string storing the object's constructor's name
       - Bytes (7+value of byte 4)-end: a series of child descriptors, stored end to end.
     - Primitive descriptors
       - Bytes 5 onwards store the data (see above)