/*
  This file is part of the MicroRender serialiser, a serialiser for the basic rendering framework
  MicroRender.
  Copyright (C) 2023-2024 Benjamin Wilkins

  MicroRender is free software: you can redistribute it and/or modify it under the terms of the
  GNU Lesser General Public License as published by the Free Software Foundation, either version 3
  of the License, or (at your option) any later version.

  MicroRender is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
  even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with MicroRender.
  If not, see <https://www.gnu.org/licenses/>.
*/

const textEncoder = new TextEncoder()
const textDecoder = new TextDecoder()

export const getEncodedLength = (text: string): number =>
  textEncoder.encode(text).length

export const encode = (text: string, bytes: DataView, offset: number): number =>
  textEncoder.encodeInto(text, new Uint8Array(bytes.buffer, bytes.byteOffset + offset)).written

export const decode = (bytes: DataView, offset: number, length: number): string =>
  textDecoder.decode(new Uint8Array(bytes.buffer, bytes.byteOffset + offset, length))