/*
  This file is part of the MicroRender serialiser, a serialiser for the basic rendering framework
  MicroRender.
  Copyright (C) 2023-2024 Benjamin Wilkins

  MicroRender is free software: you can redistribute it and/or modify it under the terms of the
  GNU Lesser General Public License as published by the Free Software Foundation, either version 3
  of the License, or (at your option) any later version.

  MicroRender is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
  even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with MicroRender.
  If not, see <https://www.gnu.org/licenses/>.
*/

import type { Constructor } from "./types.ts"

import { serialise as _serialise, getLength } from "./serialise.ts"
import { deserialise as _deserialise } from "./deserialise.ts"

/**
 * Serialise a JS object so it can be sent over HTTP or insterted as text. Creates a string that can be
 * read by `deserialise()`.
 *
 * The constructor of each object should either be in `globalThis` (and have a `name` property that matches
 * the key) or be specified in `locals`. They should be accessible to both `serialise()` and `deserialise()`.
 *
 * @param value the object or primitive to serialise.
 * @param locals an object with an entry for each object or a `Map` mapping the constructor function to its
 *   name. Note that this Map is the opposite to the one passed to `deserialise()`. If an object is passed,
 *   it is immediately converted to a Map.
 * @returns the serialised value.
 */
export /* @__PURE__ */ function serialise(value: unknown, locals: Map<Constructor, string> | object = {}): ArrayBuffer {
    let localsMap: Map<Constructor, string>

    if (locals instanceof Map) {
      localsMap = locals
    } else {
      localsMap = new Map(Object.entries(locals).map(([name, constructor]) => [constructor, name]))
    }
  
    const bytes: DataView = new DataView(new ArrayBuffer(getLength(value, localsMap)))
    _serialise(bytes, value, localsMap)

    return bytes.buffer
  }
  
  /**
   * Deserialise a JS object created by `serialise()`.
   *
   * The constructor of each object should either be in `globalThis` (and have a `name` property that matches
   * the key) or be specified in `locals`. They should be accessible to both `serialise()` and `deserialise()`.
   *
   * @param string the string to deserialise.
   * @param locals an object with an entry for each object or a `Map` mapping the name to its constructor
   *   function. Note that this Map is the opposite to the one passed to `serialise()`. If an object is passed,
   *   it is immediately converted to a Map.
   * @returns the deserialised object or primitive.
   */
  export /* @__PURE__ */ function deserialise(bytes: ArrayBuffer, locals: Map<string, Constructor> | object = {}): unknown {
    let localsMap: Map<string, Constructor>

    if (locals instanceof Map) {
      localsMap = locals
    } else {
      localsMap = new Map(Object.entries(locals))
    }
  
    return _deserialise(new DataView(bytes), localsMap)
  }