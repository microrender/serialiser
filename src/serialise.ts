/*
  This file is part of the MicroRender serialiser, a serialiser for the basic rendering framework
  MicroRender.
  Copyright (C) 2023-2024 Benjamin Wilkins

  MicroRender is free software: you can redistribute it and/or modify it under the terms of the
  GNU Lesser General Public License as published by the Free Software Foundation, either version 3
  of the License, or (at your option) any later version.

  MicroRender is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
  even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with MicroRender.
  If not, see <https://www.gnu.org/licenses/>.
*/

import { type Constructor, isIterable, isToJSON } from "./types.ts"
import { Type } from "./enums.ts"

import { encode, getEncodedLength } from "./text.ts"
import { shift } from "./shift.ts"

function sumLength(items: unknown[], localsMap: Map<Constructor, string>): number {
  return items.reduce((length: number, item: unknown) => length + getLength(item, localsMap), 0)
}
  
export function getLength(value: unknown, localsMap: Map<Constructor, string>): number {
  switch (typeof value) {
    case "string":
      return 5 + getEncodedLength(value)
    case "number":
    case "bigint":
      return 5 + 8
    case "boolean":
      return 5 + 1
    case "undefined":
      return 5
    case "object": {
      if (value === null)
        return 5

      const constructorLength: number = getEncodedLength(
        localsMap.get(value.constructor as Constructor) ?? value.constructor.name
      )
      
      if (isIterable(value)) {
        return 6 + constructorLength + sumLength([...value], localsMap)
      } else if (isToJSON(value)) {
        return 6 + constructorLength + getEncodedLength(value.toJSON())
      } else {
        return 6 + constructorLength + sumLength(Object.entries(value).flat(), localsMap)
      }
    }
  }

  throw new TypeError(`Cannot serialise type ${typeof value}`)
}

export function serialise(bytes: DataView, value: unknown, localsMap: Map<Constructor, string>): number {
  let length: number, type: Type

  switch (typeof value) {
    case "string":
      // Encode string and get length
      length = 5 + encode(value, bytes, 5)
      type = Type.string
      break
    case "number":
      length = 5 + 8
      type = Type.number
      bytes.setFloat64(5, value)
      break
    case "bigint":
      length = 5 + 8
      type = Type.bigint
      bytes.setBigInt64(5, value)
      break
    case "boolean":
      length = 5 + 1
      type = Type.boolean
      bytes.setUint8(5, value ? 1 : 0)
      break
    case "undefined":
      length = 5
      type = Type.undefined
      break
    case "object": {
      if (value === null) {
        length = 5
        type = Type.null
        break
      }

      // Encode constructor name and get length
      const constructorLength = encode(
        localsMap.get(value.constructor as Constructor) ?? value.constructor.name,
        bytes,
        6
      )

      bytes.setUint8(5, constructorLength)
      length = constructorLength + 6

      if (isToJSON(value)) {
        type = Type.ToJSON

        // Encode string and get length
        length += encode(value.toJSON(), bytes, length)
      } else {
        let iterable: Iterable<unknown>

        if (isIterable(value)) {
          type = Type.Iterable
          iterable = value
        } else {
          type = Type.BasicObject
          iterable = Object.entries(value).flat()
        }

        for (const item of iterable) {
          // Encode item and get length
          length += serialise(shift(bytes, length), item, localsMap)
        }
      }

      break
    }
    default:
      throw new TypeError("An internal error occured")
  }

  bytes.setUint32(0, length)
  bytes.setUint8(4, type)

  return length
}