/*
  This file is part of the MicroRender serialiser, a serialiser for the basic rendering framework
  MicroRender.
  Copyright (C) 2023-2024 Benjamin Wilkins

  MicroRender is free software: you can redistribute it and/or modify it under the terms of the
  GNU Lesser General Public License as published by the Free Software Foundation, either version 3
  of the License, or (at your option) any later version.

  MicroRender is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
  even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with MicroRender.
  If not, see <https://www.gnu.org/licenses/>.
*/

import type { Constructor } from "./types.ts"
import { Type } from "./enums.ts"

import { decode } from "./text.ts"
import { shift } from "./shift.ts"

export function deserialise(bytes: DataView, localsMap: Map<string, Constructor>): unknown {
  const length: number = bytes.getUint32(0)
  const type: Type = bytes.getUint8(4)

  switch (type) {
    case Type.string:
      return decode(bytes, 5, length - 5)
    case Type.number:
      return bytes.getFloat64(5)
    case Type.bigint:
      return bytes.getBigInt64(5)
    case Type.boolean:
      return bytes.getUint8(5) > 0
    case Type.undefined:
      return undefined
    case Type.null:
      return null
  }

  const constructorLength: number = bytes.getUint8(5)
  const constructorName: string = decode(bytes, 6, constructorLength)
  const constructorFn: Constructor = localsMap.get(constructorName) ?? globalThis[constructorName as keyof typeof globalThis] as Constructor

  let index: number = 6 + constructorLength

  if (type === Type.ToJSON) {
    return new constructorFn(decode(bytes, index, length - index))
  }

  const items: unknown[] = []

  while (index < length) {
    items.push(deserialise(shift(bytes, index), localsMap))
    index += bytes.getUint32(index)
  }

  if (type === Type.Iterable) {
    if (constructorFn === Array) {
      // Already an array
      return items
    } else {
      return new constructorFn(items)
    }
  } else if (type === Type.BasicObject) {
    const object = Object.create(constructorFn.prototype)
    let key: keyof typeof object = ""

    for (const [i, item] of items.entries()) {
      if (i % 2 === 0) {
        key = item as string
      } else {
        object[key] = item
      }
    }

    return object
  }
}