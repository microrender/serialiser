/*
  This file is part of the MicroRender serialiser, a serialiser for the basic rendering framework
  MicroRender.
  Copyright (C) 2023-2024 Benjamin Wilkins

  MicroRender is free software: you can redistribute it and/or modify it under the terms of the
  GNU Lesser General Public License as published by the Free Software Foundation, either version 3
  of the License, or (at your option) any later version.

  MicroRender is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
  even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with MicroRender.
  If not, see <https://www.gnu.org/licenses/>.
*/

import { assertEquals } from "jsr:@std/assert"

import { serialise, deserialise } from "../src/lib.ts"

function test(value: unknown, name: string): void {
  Deno.test({
    name,
    fn() {assertEquals(deserialise(serialise(value)), value)}
  })
}

test("hi", "string")
test(1.85235, "number")
test(198457563948756n, "bigint")
test(true, "boolean")
test(undefined, "undefined")
test(null, "null")
test({ hello: "world" }, "object")
test([1, 2, 3, 4, 5], "array")
test(new Map([["key", "value"], ["key2", "value2"]]), "map")
test(new URL("https://example.com/a/b/c"), "url")
test(new Date(0), "date")